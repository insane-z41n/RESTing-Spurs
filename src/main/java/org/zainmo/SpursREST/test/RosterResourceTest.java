package org.zainmo.SpursREST.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.zainmo.SpursREST.roster.RosterResource;

public class RosterResourceTest {

	@Test
	public void testMessage() {
		RosterResource rosterResource = new RosterResource();
		assertEquals("GO SPURS GO!", rosterResource.getTest());
	}

}
